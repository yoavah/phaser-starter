#!/usr/bin/env node
const shell = require('shelljs');
const fs = require('fs');
const xml2js = require('xml2js');
const commander = require('commander');
const webpack = require('webpack');
let webpackConfig;

commander
    .option('--dev', 'Use webpack.dev.config.js for some values, excluding this will use webpack.dist.config.js (currently only GOOGLE_WEB_FONTS is being used).')
    .parse(process.argv);

if (commander.dev) {
    webpackConfig = require('../webpack.dev.config.js');
} else {
    webpackConfig = require('../webpack.dist.config.js');
}

function toCamelCase(string) {
    if (!string) {
        return string;
    }
    let str = string.replace(/[^A-Za-z0-9]/g, ' ').replace(/^\w|[A-Z]|\b\w|\s+/g, function (match) {
        if (+match === 0 || match === '-' || match === '.') {
            return "";
        }
        return match.toUpperCase();
    });
    return str[0].toLowerCase() + str.substr(1);
}

function toPascalCase(string) {
    const camelCase = toCamelCase(string);

    return camelCase[0].toUpperCase() + camelCase.substr(1);
}

function removePrefix(prefix, filename) {
    if (filename.startsWith(prefix)) {
        let shortName = filename.substr(prefix.length);
        if (!shortName[0].match(/[a-zA-Z]/)) {
            return filename;
        }
        return shortName;
    }
    return filename;
}

function findExtension(haystack, arr) {
    return arr.some(function (v) {
        return haystack.lastIndexOf(v) >= 0;
    });
}

const gameAssets = {};

const loaderTypes = {
    image: {},
    spritesheet: {},
    atlas: {},
    audio: {},
    audiosprite: {},
    font: {},
    bitmap_font: {},
    json: {},
    xml: {},
    text: {},
    script: {},
    shader: {},
    misc: {}
};

const audioExtensions = ['aac', 'ac3', 'caf', 'flac', 'm4a', 'mp3', 'mp4', 'ogg', 'wav', 'webm'];
const imageExtensions = ['bmp', 'gif', 'jpg', 'jpeg', 'png', 'webp'];
const fontExtensions = ['eot', 'otf', 'svg', 'ttf', 'woff', 'woff2'];
const bitmapFontExtensions = ['xml', 'fnt'];
const jsonExtensions = ['json'];
const xmlExtensions = ['xml'];
const textExtensions = ['txt'];
const scriptExtensions = ['js'];
const shaderExtensions = ['frag'];

shell.ls('assets/**/*.*').forEach(function (file) {
    const filePath = file.replace('assets/', '');
    const fileName = filePath.substring(0, filePath.lastIndexOf('.'));
    const extension = filePath.substr(filePath.lastIndexOf('.') + 1);
    const gameAsset = gameAssets[fileName] || [];
    gameAssets[fileName] = gameAsset.concat(extension);
});

for (let i in gameAssets) {
    const imageType = findExtension(gameAssets[i], imageExtensions);
    const audioType = findExtension(gameAssets[i], audioExtensions);
    const fontType = findExtension(gameAssets[i], fontExtensions);
    let bitmapFontType = findExtension(gameAssets[i], bitmapFontExtensions);
    const jsonType = findExtension(gameAssets[i], jsonExtensions);
    const xmlType = findExtension(gameAssets[i], xmlExtensions);
    const textType = findExtension(gameAssets[i], textExtensions);
    const scriptType = findExtension(gameAssets[i], scriptExtensions);
    const shaderType = findExtension(gameAssets[i], shaderExtensions);

    if (bitmapFontType) {
        let isItActuallyAFont = false;

        for (const ext in gameAssets[i]) {
            if (gameAssets[i].hasOwnProperty(ext)) {
                if (((shell.grep(/^[\s\S]*?<font>/g, ('assets/' + i + '.' + gameAssets[i][ext]))).length > 1)) {
                    isItActuallyAFont = true;
                    break;
                }
            }
        }

        bitmapFontType = isItActuallyAFont;
    }

    if (bitmapFontType && imageType) {
        loaderTypes.bitmap_font[i] = gameAssets[i];
    } else if (audioType) {
        if (jsonType) {
            loaderTypes.audiosprite[i] = gameAssets[i];
        } else {
            loaderTypes.audio[i] = gameAssets[i];
        }
    } else if (imageType) {
        if (jsonType || xmlType) {
            loaderTypes.atlas[i] = gameAssets[i];
        } else {
            const spritesheetData = i.match(/\[(-?[0-9],?)*]/);
            if (spritesheetData && spritesheetData.length > 0) {
                loaderTypes.spritesheet[i] = gameAssets[i];
            } else {
                loaderTypes.image[i] = gameAssets[i];
            }
        }
    } else if (fontType) {
        loaderTypes.font[i] = gameAssets[i];
    } else if (jsonType) {
        loaderTypes.json[i] = gameAssets[i];
    } else if (xmlType) {
        loaderTypes.xml[i] = gameAssets[i];
    } else if (textType) {
        loaderTypes.text[i] = gameAssets[i];
    } else if (scriptType) {
        loaderTypes.script[i] = gameAssets[i];
    } else if (shaderType) {
        loaderTypes.shader[i] = gameAssets[i];
    } else {
        loaderTypes.misc[i] = gameAssets[i];
    }
}

const assetsClassFile = 'src/assets.ts';
shell.rm('-f', assetsClassFile);

shell.ShellString('/* AUTO GENERATED FILE. DO NOT MODIFY. YOU WILL LOSE YOUR CHANGES ON BUILD. */\n\n').to(assetsClassFile);

shell.ShellString('export namespace Images {').toEnd(assetsClassFile);
if (!Object.keys(loaderTypes.image).length) {
    shell.ShellString('\n    class IExistSoTypeScriptWillNotComplainAboutAnEmptyNamespace {}').toEnd(assetsClassFile);
} else {
    for (let i in loaderTypes.image) {
        shell.ShellString('\n    export const ' + toCamelCase(removePrefix('images/', i)) + ' = {').toEnd(assetsClassFile);
        shell.ShellString('\n       name: \'' + i.split('/').pop() + '\',\n').toEnd(assetsClassFile);

        for (let t in loaderTypes.image[i]) {
            shell.ShellString('\n       ' + toCamelCase(loaderTypes.image[i][t]) + ': require(\'assets/' + i + '.' + loaderTypes.image[i][t] + '\') as string,').toEnd(assetsClassFile);
        }

        shell.ShellString('\n    };\n').toEnd(assetsClassFile);
    }
}
shell.ShellString('\n}\n\n').toEnd(assetsClassFile);

shell.ShellString('export namespace SpriteSheets {').toEnd(assetsClassFile);
if (!Object.keys(loaderTypes.spritesheet).length) {
    shell.ShellString('\n    class IExistSoTypeScriptWillNotComplainAboutAnEmptyNamespace {}').toEnd(assetsClassFile);
} else {
    for (let i in loaderTypes.spritesheet) {
        shell.ShellString('\n    export const ' + toCamelCase(removePrefix('spritesheets/', i)) + '  = {').toEnd(assetsClassFile);
        shell.ShellString('\n        name: \'' + i.split('/').pop() + '\',\n').toEnd(assetsClassFile);

        shell.ShellString('\n        ' + toCamelCase(loaderTypes.spritesheet[i][0]) + ': require(\'assets/' + i + '.' + loaderTypes.spritesheet[i][0] + (loaderTypes.spritesheet[i][1] ? '.' + loaderTypes.spritesheet[i][1] : '') + '\') as string,').toEnd(assetsClassFile);

        const spritesheetProperties = i.split('.')[1].replace('[', '').replace(']', '').split(',');
        if (spritesheetProperties.length < 2 || spritesheetProperties.length > 5) {
            console.log('Invalid number of SpriteSheet properties provided for \'' + i + '\'. Must have between 2 and 5; [frameWidth, frameHeight, frameMax, margin, spacing] frameWidth and frameHeight are required');
        }

        shell.ShellString('\n        frameWidth: ' + parseInt(spritesheetProperties[0] ? spritesheetProperties[0] : -1) + ',').toEnd(assetsClassFile);
        shell.ShellString('\n        frameHeight: ' + parseInt(spritesheetProperties[1] ? spritesheetProperties[1] : -1) + ',').toEnd(assetsClassFile);
        shell.ShellString('\n        frameMax: ' + parseInt(spritesheetProperties[2] ? spritesheetProperties[2] : -1) + ',').toEnd(assetsClassFile);
        shell.ShellString('\n        margin: ' + parseInt(spritesheetProperties[3] ? spritesheetProperties[3] : 0) + ',').toEnd(assetsClassFile);
        shell.ShellString('\n        spacing: ' + parseInt(spritesheetProperties[4] ? spritesheetProperties[4] : 0) + ',').toEnd(assetsClassFile);

        shell.ShellString('\n    };\n').toEnd(assetsClassFile);
    }
}
shell.ShellString('\n}\n\n').toEnd(assetsClassFile);

shell.ShellString('export namespace Atlases {').toEnd(assetsClassFile);
if (!Object.keys(loaderTypes.atlas).length) {
    shell.ShellString('\n    class IExistSoTypeScriptWillNotComplainAboutAnEmptyNamespace {}').toEnd(assetsClassFile);
} else {
    for (let i in loaderTypes.atlas) {
        const dataExtensions = [];
        const dataTypes = [];

        const enumName = toPascalCase(i) + 'Frames';
        for (let t in loaderTypes.atlas[i]) {
            let dataFile = ('assets/' + i + '.' + loaderTypes.atlas[i][t]);
            let fileData = null;
            let json = null;
            let parser = null;
            let frameFull = '';
            let frame = '';
            let indexOfExtension = -1;

            dataExtensions.push(loaderTypes.atlas[i][t]);

            if (jsonExtensions.indexOf(loaderTypes.atlas[i][t]) !== -1) {
                shell.ShellString('\n    enum ' + enumName + ' {').toEnd(assetsClassFile);

                try {
                    fileData = fs.readFileSync(dataFile, 'ascii');
                    json = JSON.parse(fileData);

                    if (Array.isArray(json['frames'])) {
                        dataTypes.push('Array');

                        for (let a in json['frames']) {
                            frameFull = (json['frames'][a]['filename']);
                            indexOfExtension = frameFull.lastIndexOf('.');
                            if (indexOfExtension === -1) {
                                frame = frameFull;
                            } else {
                                frame = frameFull.substring(0, indexOfExtension);
                            }
                            shell.ShellString('\n        ' + toCamelCase(frame) + ' = \'' + frameFull + '\',').toEnd(assetsClassFile);
                        }
                    } else {
                        dataTypes.push('Hash');

                        for (let h in json['frames']) {
                            frameFull = (h);
                            indexOfExtension = frameFull.lastIndexOf('.');
                            if (indexOfExtension === -1) {
                                frame = frameFull;
                            } else {
                                frame = frameFull.substring(0, indexOfExtension);
                            }
                            shell.ShellString('\n        ' + toCamelCase(frame) + ' = \'' + frameFull + '\',').toEnd(assetsClassFile);
                        }
                    }
                } catch (e) {
                    console.log('Atlas Data File Error: ', e);
                }

                shell.ShellString('\n    }\n').toEnd(assetsClassFile);
            } else if (xmlExtensions.indexOf(loaderTypes.atlas[i][t]) !== -1) {
                dataTypes.push('');

                shell.ShellString('\n    enum ' + enumName + ' {').toEnd(assetsClassFile);

                try {
                    fileData = fs.readFileSync(dataFile, 'ascii');
                    parser = new xml2js.Parser();

                    parser.parseString(fileData.substring(0, fileData.length), function (err, result) {
                        for (let x in result['TextureAtlas']['SubTexture']) {
                            frameFull = (result['TextureAtlas']['SubTexture'][x]['$']['name']);
                            indexOfExtension = frameFull.lastIndexOf('.');
                            if (indexOfExtension === -1) {
                                frame = frameFull;
                            } else {
                                frame = frameFull.substring(0, indexOfExtension);
                            }
                            shell.ShellString('\n        ' + toPascalCase(frame) + ' = <any>\'' + frameFull + '\',').toEnd(assetsClassFile);
                        }
                    });
                } catch (e) {
                    console.log('Atlas Data File Error: ', e);
                }

                shell.ShellString('\n    }').toEnd(assetsClassFile);
            } else {
                dataTypes.push('');
            }
        }

        shell.ShellString('\n    export const ' + toCamelCase(removePrefix('atlases/', i)) + ' = {').toEnd(assetsClassFile);
        shell.ShellString('\n        name: \'' + i.split('/').pop() + '\',').toEnd(assetsClassFile);
        for (let e in dataExtensions) {
            shell.ShellString('\n        ' + toCamelCase(dataExtensions[e]) + dataTypes[e] + ': require(\'assets/' + i + '.' + dataExtensions[e] + '\') as string,').toEnd(assetsClassFile);
        }
        shell.ShellString('\n        frames: ' + enumName + ',').toEnd(assetsClassFile);
        shell.ShellString('\n    };\n').toEnd(assetsClassFile);
    }
}
shell.ShellString('\n}\n\n').toEnd(assetsClassFile);

shell.ShellString('export namespace Audio {').toEnd(assetsClassFile);
if (!Object.keys(loaderTypes.audio).length) {
    shell.ShellString('\n    class IExistSoTypeScriptWillNotComplainAboutAnEmptyNamespace {}').toEnd(assetsClassFile);
} else {
    for (let i in loaderTypes.audio) {
        shell.ShellString('\n    export const ' + toCamelCase(removePrefix('audio/', i)) + ' = {').toEnd(assetsClassFile);
        shell.ShellString('\n        name: \'' + i.split('/').pop() + '\',\n').toEnd(assetsClassFile);

        for (let t in loaderTypes.audio[i]) {
            shell.ShellString('\n        ' + toCamelCase(loaderTypes.audio[i][t]) + ': require(\'assets/' + i + '.' + loaderTypes.audio[i][t] + '\') as string,').toEnd(assetsClassFile);
        }

        shell.ShellString('\n    };\n').toEnd(assetsClassFile);
    }
}
shell.ShellString('\n}\n\n').toEnd(assetsClassFile);

shell.ShellString('export namespace AudioSprites {').toEnd(assetsClassFile);
if (!Object.keys(loaderTypes.audiosprite).length) {
    shell.ShellString('\n    class IExistSoTypeScriptWillNotComplainAboutAnEmptyNamespace {}').toEnd(assetsClassFile);
} else {
    for (let i in loaderTypes.audiosprite) {
        for (let t in loaderTypes.audiosprite[i]) {
            let dataFile = ('assets/' + i + '.' + loaderTypes.audiosprite[i][t]);
            let fileData = null;
            let json = null;
            let sprite = null;

            if (jsonExtensions.indexOf(loaderTypes.audiosprite[i][t]) !== -1) {
                shell.ShellString('\n    enum ' + toPascalCase(i) + 'Sprites {').toEnd(assetsClassFile);

                try {
                    fileData = fs.readFileSync(dataFile, 'ascii');
                    json = JSON.parse(fileData);

                    for (let h in json['spritemap']) {
                        sprite = (h);
                        shell.ShellString('\n        ' + toPascalCase(sprite) + ' = \'' + sprite + '\',').toEnd(assetsClassFile);
                    }
                } catch (e) {
                    console.log('Audiosprite Data File Error: ', e);
                }

                shell.ShellString('\n    }\n').toEnd(assetsClassFile);
            }
        }

        shell.ShellString('\n    export const ' + toCamelCase(removePrefix('audiosprites/', i)) + ' = {').toEnd(assetsClassFile);
        shell.ShellString('\n        name: \'' + i.split('/').pop() + '\',\n').toEnd(assetsClassFile);
        for (let t in loaderTypes.audiosprite[i]) {
            shell.ShellString('\n        ' + toCamelCase(loaderTypes.audiosprite[i][t]) + ': require(\'assets/' + i + '.' + loaderTypes.audiosprite[i][t] + '\') as string,').toEnd(assetsClassFile);
        }
        shell.ShellString('\n\n        sprites: ' + toPascalCase(i) + 'Sprites,').toEnd(assetsClassFile);
        shell.ShellString('\n    };\n').toEnd(assetsClassFile);
    }
}
shell.ShellString('\n}\n\n').toEnd(assetsClassFile);

shell.ShellString('export namespace GoogleWebFonts {').toEnd(assetsClassFile);
const webFontsToUse = JSON.parse(webpackConfig.plugins[webpackConfig.plugins.findIndex(function (element) {
    return (element instanceof webpack.DefinePlugin);
})].definitions.GOOGLE_WEB_FONTS);
if (!webFontsToUse.length) {
    shell.ShellString('\n    class IExistSoTypeScriptWillNotComplainAboutAnEmptyNamespace {}').toEnd(assetsClassFile);
} else {
    for (let i in webFontsToUse) {
        shell.ShellString('\n    export const ' + toPascalCase(webFontsToUse[i]) + ' = \'' + webFontsToUse[i] + '\';').toEnd(assetsClassFile);
    }
}
shell.ShellString('\n}\n\n').toEnd(assetsClassFile);


shell.ShellString('export namespace CustomWebFonts {').toEnd(assetsClassFile);
if (!Object.keys(loaderTypes.font).length) {
    shell.ShellString('\n    class IExistSoTypeScriptWillNotComplainAboutAnEmptyNamespace {}').toEnd(assetsClassFile);
} else {
    for (let i in loaderTypes.font) {
        shell.ShellString('\n    export const ' + toCamelCase(removePrefix('fonts/', i)) + ' = {').toEnd(assetsClassFile);
        shell.ShellString('\n        name: \'' + i.split('/').pop() + '\',\n').toEnd(assetsClassFile);

        const cssFileData = fs.readFileSync(('assets/' + i + '.css'), 'ascii');
        const family = /font-family:(\s)*(['"])(\w*\W*)(['"])/g.exec(cssFileData)[3];
        shell.ShellString('\n        family: \'' + family + '\',\n').toEnd(assetsClassFile);

        for (let t in loaderTypes.font[i]) {
            shell.ShellString('\n        ' + toCamelCase(loaderTypes.font[i][t]) + ': require(\'!file-loader?name=assets/fonts/[name].[ext]!assets/' + i + '.' + loaderTypes.font[i][t] + '\') as string,').toEnd(assetsClassFile);
        }

        shell.ShellString('\n    };\n').toEnd(assetsClassFile);
    }
}
shell.ShellString('\n}\n\n').toEnd(assetsClassFile);

shell.ShellString('export namespace BitmapFonts {').toEnd(assetsClassFile);
if (!Object.keys(loaderTypes.bitmap_font).length) {
    shell.ShellString('\n    class IExistSoTypeScriptWillNotComplainAboutAnEmptyNamespace {}').toEnd(assetsClassFile);
} else {
    for (let i in loaderTypes.bitmap_font) {
        shell.ShellString('\n    export const ' + toCamelCase(removePrefix('fonts/', i)) + ' = {').toEnd(assetsClassFile);
        shell.ShellString('\n        name: \'' + i.split('/').pop() + '\',\n').toEnd(assetsClassFile);

        for (let t in loaderTypes.bitmap_font[i]) {
            shell.ShellString('\n        ' + toCamelCase(loaderTypes.bitmap_font[i][t]) + ': require(\'assets/' + i + '.' + loaderTypes.bitmap_font[i][t] + '\') as string,').toEnd(assetsClassFile);
        }

        shell.ShellString('\n    };\n').toEnd(assetsClassFile);
    }
}
shell.ShellString('\n}\n\n').toEnd(assetsClassFile);

shell.ShellString('export namespace JSON {').toEnd(assetsClassFile);
if (!Object.keys(loaderTypes.json).length) {
    shell.ShellString('\n    class IExistSoTypeScriptWillNotComplainAboutAnEmptyNamespace {}').toEnd(assetsClassFile);
} else {
    for (let i in loaderTypes.json) {
        shell.ShellString('\n    export const ' + toCamelCase(removePrefix('data/', i)) + ' = {').toEnd(assetsClassFile);
        shell.ShellString('\n        name: \'' + i.split('/').pop() + '\',').toEnd(assetsClassFile);

        for (let t in loaderTypes.json[i]) {
            shell.ShellString('\n        ' + toCamelCase(loaderTypes.json[i][t]) + ': require(\'assets/' + i + '.' + loaderTypes.json[i][t] + '\') as string,').toEnd(assetsClassFile);
        }

        shell.ShellString('\n    };\n').toEnd(assetsClassFile);
    }
}
shell.ShellString('\n}\n\n').toEnd(assetsClassFile);

shell.ShellString('export namespace XML {').toEnd(assetsClassFile);
if (!Object.keys(loaderTypes.xml).length) {
    shell.ShellString('\n    class IExistSoTypeScriptWillNotComplainAboutAnEmptyNamespace {}').toEnd(assetsClassFile);
} else {
    for (let i in loaderTypes.xml) {
        shell.ShellString('\n    export const ' + toCamelCase(removePrefix('data/', i)) + ' = {').toEnd(assetsClassFile);
        shell.ShellString('\n        name: \'' + i.split('/').pop() + '\',').toEnd(assetsClassFile);

        for (let t in loaderTypes.xml[i]) {
            shell.ShellString('\n        ' + toCamelCase(loaderTypes.xml[i][t]) + ': require(\'assets/' + i + '.' + loaderTypes.xml[i][t] + '\') as string,').toEnd(assetsClassFile);
        }

        shell.ShellString('\n    };\n').toEnd(assetsClassFile);
    }
}
shell.ShellString('\n}\n\n').toEnd(assetsClassFile);

shell.ShellString('export namespace Text {').toEnd(assetsClassFile);
if (!Object.keys(loaderTypes.text).length) {
    shell.ShellString('\n    class IExistSoTypeScriptWillNotComplainAboutAnEmptyNamespace {}').toEnd(assetsClassFile);
} else {
    for (let i in loaderTypes.text) {
        shell.ShellString('\n    export const ' + toCamelCase(removePrefix('text/', i)) + ' = {').toEnd(assetsClassFile);
        shell.ShellString('\n        name: \'' + i.split('/').pop() + '\',\n').toEnd(assetsClassFile);

        for (let t in loaderTypes.text[i]) {
            shell.ShellString('\n        ' + toCamelCase(loaderTypes.text[i][t]) + ': require(\'assets/' + i + '.' + loaderTypes.text[i][t] + '\') as string,').toEnd(assetsClassFile);
        }

        shell.ShellString('\n    };\n').toEnd(assetsClassFile);
    }
}
shell.ShellString('\n}\n\n').toEnd(assetsClassFile);

shell.ShellString('export namespace Scripts {').toEnd(assetsClassFile);
if (!Object.keys(loaderTypes.script).length) {
    shell.ShellString('\n    class IExistSoTypeScriptWillNotComplainAboutAnEmptyNamespace {}').toEnd(assetsClassFile);
} else {
    for (let i in loaderTypes.script) {
        shell.ShellString('\n    export const ' + toCamelCase(removePrefix('scripts/', i)) + ' = {').toEnd(assetsClassFile);
        shell.ShellString('\n        name: \'' + i.split('/').pop() + '\',\n').toEnd(assetsClassFile);

        for (let t in loaderTypes.script[i]) {
            shell.ShellString('\n        ' + toCamelCase(loaderTypes.script[i][t]) + ': require(\'assets/' + i + '.' + loaderTypes.script[i][t] + '\') as string,').toEnd(assetsClassFile);
        }

        shell.ShellString('\n    };\n').toEnd(assetsClassFile);
    }
}
shell.ShellString('\n}\n').toEnd(assetsClassFile);

shell.ShellString('export namespace Shaders {').toEnd(assetsClassFile);
if (!Object.keys(loaderTypes.shader).length) {
    shell.ShellString('\n    class IExistSoTypeScriptWillNotComplainAboutAnEmptyNamespace {}').toEnd(assetsClassFile);
} else {
    for (let i in loaderTypes.shader) {
        shell.ShellString('\n    export const ' + toCamelCase(removePrefix('shaders/', i)) + ' = {').toEnd(assetsClassFile);
        shell.ShellString('\n        name: \'' + i.split('/').pop() + '\',\n').toEnd(assetsClassFile);

        for (let t in loaderTypes.shader[i]) {
            shell.ShellString('\n        ' + toCamelCase(loaderTypes.shader[i][t]) + ': require(\'assets/' + i + '.' + loaderTypes.shader[i][t] + '\') as string,').toEnd(assetsClassFile);
        }

        shell.ShellString('\n    };\n').toEnd(assetsClassFile);
    }
}
shell.ShellString('\n}\n\n').toEnd(assetsClassFile);

shell.ShellString('export namespace Misc {').toEnd(assetsClassFile);
if (!Object.keys(loaderTypes.misc).length) {
    shell.ShellString('\n    class IExistSoTypeScriptWillNotComplainAboutAnEmptyNamespace {}').toEnd(assetsClassFile);
} else {
    for (let i in loaderTypes.misc) {
        shell.ShellString('\n    export const ' + toCamelCase(i) + ' = {').toEnd(assetsClassFile);
        shell.ShellString('\n        name: \'' + i.split('/').pop() + '\',\n').toEnd(assetsClassFile);

        for (let t in loaderTypes.misc[i]) {
            shell.ShellString('\n        file: require(\'assets/' + i + '.' + loaderTypes.misc[i][t] + '\') as string,').toEnd(assetsClassFile);
        }

        shell.ShellString('\n    }').toEnd(assetsClassFile);
    }
}
shell.ShellString('\n}\n').toEnd(assetsClassFile);
