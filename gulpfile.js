const gulp = require('gulp');
const watch = require('gulp-watch');
const shell = require('gulp-shell');

gulp.task('assets', function () {
    // Endless stream mode
    watch(__dirname + '/scripts/generateAssetsClass.js', {ignoreInitial: false})
        .pipe(shell(__dirname + '/scripts/generateAssetsClass.js', {verbose: true}));

    return watch('assets/**', {ignoreInitial: true})
        .pipe(shell(__dirname + '/scripts/generateAssetsClass.js', {verbose: true}));
});
