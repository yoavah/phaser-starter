/* AUTO GENERATED FILE. DO NOT MODIFY. YOU WILL LOSE YOUR CHANGES ON BUILD. */

export namespace Images {
    export const backgroundTemplate = {
       name: 'background_template',

       png: require('assets/images/background_template.png') as string,
    };

}

export namespace SpriteSheets {
    export const metalslugMummy374518  = {
        name: 'metalslug_mummy.[37,45,18,0,0]',

        png: require('assets/spritesheets/metalslug_mummy.[37,45,18,0,0].png') as string,
        frameWidth: 37,
        frameHeight: 45,
        frameMax: 18,
        margin: 0,
        spacing: 0,
    };

}

export namespace Atlases {
    enum AtlasesKnightFrames {
        attack1 = 'attack1',
        attack10 = 'attack10',
        attack2 = 'attack2',
        attack3 = 'attack3',
        attack4 = 'attack4',
        attack5 = 'attack5',
        attack6 = 'attack6',
        attack7 = 'attack7',
        attack8 = 'attack8',
        attack9 = 'attack9',
    }

    export const knight = {
        name: 'knight',
        jsonHash: require('assets/atlases/knight.json') as string,
        png: require('assets/atlases/knight.png') as string,
        frames: AtlasesKnightFrames,
    };


    enum AtlasesPreloadSpritesArrayFrames {
        preloadBar = 'preload_bar.png',
        preloadFrame = 'preload_frame.png',
    }

    export const preloadSpritesArray = {
        name: 'preload_sprites_array',
        jsonArray: require('assets/atlases/preload_sprites_array.json') as string,
        png: require('assets/atlases/preload_sprites_array.png') as string,
        frames: AtlasesPreloadSpritesArrayFrames,
    };

    enum AtlasesPreloadSpritesHashFrames {
        preloadBar = 'preload_bar.png',
        preloadFrame = 'preload_frame.png',
    }

    export const preloadSpritesHash = {
        name: 'preload_sprites_hash',
        jsonHash: require('assets/atlases/preload_sprites_hash.json') as string,
        png: require('assets/atlases/preload_sprites_hash.png') as string,
        frames: AtlasesPreloadSpritesHashFrames,
    };

    enum AtlasesPreloadSpritesXmlFrames {
        PreloadBar = <any>'preload_bar.png',
        PreloadFrame = <any>'preload_frame.png',
    }
    export const preloadSpritesXml = {
        name: 'preload_sprites_xml',
        png: require('assets/atlases/preload_sprites_xml.png') as string,
        xml: require('assets/atlases/preload_sprites_xml.xml') as string,
        frames: AtlasesPreloadSpritesXmlFrames,
    };

}

export namespace Audio {
    export const music = {
        name: 'music',

        ac3: require('assets/audio/music.ac3') as string,
        m4a: require('assets/audio/music.m4a') as string,
        mp3: require('assets/audio/music.mp3') as string,
        ogg: require('assets/audio/music.ogg') as string,
    };

}

export namespace AudioSprites {
    enum AudiospritesLasersSfxSprites {
        Laser1 = 'laser1',
        Laser2 = 'laser2',
        Laser3 = 'laser3',
        Laser4 = 'laser4',
        Laser5 = 'laser5',
        Laser6 = 'laser6',
        Laser7 = 'laser7',
        Laser8 = 'laser8',
        Laser9 = 'laser9',
    }

    export const lasersSfx = {
        name: 'sfx',

        ac3: require('assets/audiosprites/lasers/sfx.ac3') as string,
        json: require('assets/audiosprites/lasers/sfx.json') as string,
        m4a: require('assets/audiosprites/lasers/sfx.m4a') as string,
        mp3: require('assets/audiosprites/lasers/sfx.mp3') as string,
        ogg: require('assets/audiosprites/lasers/sfx.ogg') as string,

        sprites: AudiospritesLasersSfxSprites,
    };

}

export namespace GoogleWebFonts {
    export const Barrio = 'Barrio';
}

export namespace CustomWebFonts {
    export const fonts2DumbWebfont = {
        name: '2Dumb-webfont',

        family: '2dumbregular',

        css: require('!file-loader?name=assets/fonts/[name].[ext]!assets/fonts/2Dumb-webfont.css') as string,
        eot: require('!file-loader?name=assets/fonts/[name].[ext]!assets/fonts/2Dumb-webfont.eot') as string,
        svg: require('!file-loader?name=assets/fonts/[name].[ext]!assets/fonts/2Dumb-webfont.svg') as string,
        ttf: require('!file-loader?name=assets/fonts/[name].[ext]!assets/fonts/2Dumb-webfont.ttf') as string,
        woff: require('!file-loader?name=assets/fonts/[name].[ext]!assets/fonts/2Dumb-webfont.woff') as string,
    };

}

export namespace BitmapFonts {
    export const fontXml = {
        name: 'font_xml',

        png: require('assets/fonts/font_xml.png') as string,
        xml: require('assets/fonts/font_xml.xml') as string,
    };

}

export namespace JSON {
    class IExistSoTypeScriptWillNotComplainAboutAnEmptyNamespace {}
}

export namespace XML {
    class IExistSoTypeScriptWillNotComplainAboutAnEmptyNamespace {}
}

export namespace Text {
    class IExistSoTypeScriptWillNotComplainAboutAnEmptyNamespace {}
}

export namespace Scripts {
    export const blurX = {
        name: 'BlurX',

        js: require('assets/scripts/BlurX.js') as string,
    };

    export const blurY = {
        name: 'BlurY',

        js: require('assets/scripts/BlurY.js') as string,
    };

}
export namespace Shaders {
    export const pixelate = {
        name: 'pixelate',

        frag: require('assets/shaders/pixelate.frag') as string,
    };

}

export namespace Misc {
    class IExistSoTypeScriptWillNotComplainAboutAnEmptyNamespace {}
}
