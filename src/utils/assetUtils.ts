import * as _Assets from '../assets';

export const Assets = _Assets;

export class Loader {
    private static game: Phaser.Game = null;
    private static soundKeys: string[] = [];
    private static soundExtensionsPreference: string[] = SOUND_EXTENSIONS_PREFERENCE;

    public static loadAllAssets(game: Phaser.Game, onComplete?: Function, onCompleteContext?: any) {
        this.game = game;

        if (onComplete) {
            this.game.load.onLoadComplete.addOnce(onComplete, onCompleteContext);
        }

        this.loadImages();
        this.loadSpriteSheets();
        this.loadAtlases();
        this.loadAudio();
        this.loadAudioSprites();
        this.loadBitmapFonts();
        this.loadJSON();
        this.loadXML();
        this.loadText();
        this.loadScripts();
        this.loadShaders();
        this.loadMisc();
    }

    public static waitForSoundDecoding(onComplete: Function, onCompleteContext?: any) {
        if (this.soundKeys.length > 0) {
            this.game.sound.setDecodedCallback(this.soundKeys, onComplete, onCompleteContext);
        } else {
            onComplete.call(onCompleteContext);
        }
    }

    private static loadImages() {
        for (let name in Assets.Images) {
            if (Assets.Images.hasOwnProperty(name)) {
                const image = Assets.Images[name];
                if (!this.game.cache.checkImageKey(image.name)) {
                    for (let option of Object.getOwnPropertyNames(image)) {
                        if (option !== 'name') {
                            this.game.load.image(image.name, image[option]);
                        }
                    }
                }
            }
        }
    }

    private static loadSpriteSheets() {
        for (let name in Assets.SpriteSheets) {
            if (Assets.SpriteSheets.hasOwnProperty(name)) {
                const spritesheet = Assets.SpriteSheets[name];
                if (!this.game.cache.checkImageKey(spritesheet.name)) {
                    let imageOption = null;

                    for (let option of Object.getOwnPropertyNames(spritesheet)) {
                        if (option !== 'name' && option !== 'frameWidth' && option !== 'frameHeight' && option !== 'frameMax' && option !== 'margin' && option !== 'spacing') {
                            imageOption = option;
                        }
                    }
                    this.game.load.spritesheet(
                        spritesheet.name,
                        spritesheet[imageOption],
                        spritesheet.frameWidth,
                        spritesheet.frameHeight,
                        spritesheet.frameMax,
                        spritesheet.margin,
                        spritesheet.spacing);
                }
            }
        }
    }

    private static loadAtlases() {
        for (let name in Assets.Atlases) {
            if (Assets.Atlases.hasOwnProperty(name)) {
                const atlas = Assets.Atlases[name];
                if (!this.game.cache.checkImageKey(atlas.name)) {
                    let imageOption = null;
                    let dataOption = null;

                    for (let option of Object.getOwnPropertyNames(atlas)) {
                        if ((option === 'xml' || option === 'jsonArray' || option === 'jsonHash')) {
                            dataOption = option;
                        } else if (option !== 'name' && option !== 'frames') {
                            imageOption = option;
                        }
                    }

                    if (dataOption === 'xml') {
                        this.game.load.atlasXML(atlas.name, atlas[imageOption], atlas.xml);
                    } else if (dataOption === 'jsonArray') {
                        this.game.load.atlasJSONArray(atlas.name, atlas[imageOption], atlas.jsonArray);
                    } else if (dataOption === 'jsonHash') {
                        this.game.load.atlasJSONHash(atlas.name, atlas[imageOption], atlas.jsonHash);
                    }
                }
            }
        }
    }

    private static orderAudioSourceArrayBasedOnSoundExtensionPreference(soundSourceArray: string[]): string[] {
        let orderedSoundSourceArray: string[] = [];

        for (let e in this.soundExtensionsPreference) {
            const sourcesWithExtension: string[] = soundSourceArray.filter((el) => {
                return (el.substring(el.lastIndexOf('.') + 1, el.length) === this.soundExtensionsPreference[e]);
            });

            orderedSoundSourceArray = orderedSoundSourceArray.concat(sourcesWithExtension);
        }

        return orderedSoundSourceArray;
    }

    private static loadAudio() {
        for (let name in Assets.Audio) {
            if (Assets.Audio.hasOwnProperty(name)) {
                const audio = Assets.Audio[name];
                const soundName = audio.name;
                this.soundKeys.push(soundName);

                if (!this.game.cache.checkSoundKey(soundName)) {
                    let audioTypeArray = [];

                    for (let option of Object.getOwnPropertyNames(audio)) {
                        if (option !== 'name') {
                            audioTypeArray.push(audio[option]);
                        }
                    }

                    audioTypeArray = this.orderAudioSourceArrayBasedOnSoundExtensionPreference(audioTypeArray);

                    this.game.load.audio(soundName, audioTypeArray, true);
                }
            }
        }
    }

    private static loadAudioSprites() {
        for (let name in Assets.AudioSprites) {
            if (Assets.AudioSprites.hasOwnProperty(name)) {
                const audioSprite = Assets.AudioSprites[name];
                const soundName = audioSprite.name;
                this.soundKeys.push(soundName);

                if (!this.game.cache.checkSoundKey(soundName)) {
                    let audioTypeArray = [];

                    for (let option of Object.getOwnPropertyNames(audioSprite)) {
                        if (option !== 'name' && option !== 'json' && option !== 'sprites') {
                            audioTypeArray.push(audioSprite[option]);
                        }
                    }

                    audioTypeArray = this.orderAudioSourceArrayBasedOnSoundExtensionPreference(audioTypeArray);

                    this.game.load.audiosprite(soundName, audioTypeArray, audioSprite.json, null, true);
                }
            }
        }
    }

    private static loadBitmapFonts() {
        for (let name in Assets.BitmapFonts) {
            if (Assets.BitmapFonts.hasOwnProperty(name)) {

                const bitmapFont = Assets.BitmapFonts[name];
                if (!this.game.cache.checkBitmapFontKey(bitmapFont.name)) {
                    let imageOption = null;
                    let dataOption = null;

                    for (let option of Object.getOwnPropertyNames(bitmapFont)) {
                        if ((option === 'xml' || option === 'fnt')) {
                            dataOption = option;
                        } else if (option !== 'name') {
                            imageOption = option;
                        }
                    }

                    this.game.load.bitmapFont(bitmapFont.name, bitmapFont[imageOption], bitmapFont[dataOption]);
                }
            }
        }
    }

    private static loadJSON() {
        for (let name in Assets.JSON) {
            if (Assets.JSON.hasOwnProperty(name)) {
                const json = Assets.JSON[name];
                if (!this.game.cache.checkJSONKey(json.name)) {
                    this.game.load.json(json.name, json.json, true);
                }
            }
        }
    }

    private static loadXML() {
        for (let name in Assets.XML) {
            if (Assets.XML.hasOwnProperty(name)) {
                const xml = Assets.XML[name];
                if (!this.game.cache.checkXMLKey(xml.name)) {
                    this.game.load.xml(xml.name, xml.xml, true);
                }
            }
        }
    }

    private static loadText() {
        for (let name in Assets.Text) {
            if (Assets.Text.hasOwnProperty(name)) {
                const text = Assets.Text[name];
                if (!this.game.cache.checkTextKey(text.name)) {
                    this.game.load.text(text.name, text.txt, true);
                }
            }
        }
    }

    private static loadScripts() {
        for (let name in Assets.Scripts) {
            if (Assets.Scripts.hasOwnProperty(name)) {
                const script = Assets.Scripts[name];
                this.game.load.script(script.name, script.js);
            }
        }
    }

    private static loadShaders() {
        for (let name in Assets.Shaders) {
            if (Assets.Shaders.hasOwnProperty(name)) {
                const shader = Assets.Shaders[name];
                if (!this.game.cache.checkShaderKey(shader.name)) {
                    this.game.load.shader(shader.name, shader.frag, true);
                }
            }
        }
    }

    private static loadMisc() {
        for (let name in Assets.Misc) {
            if (Assets.Misc.hasOwnProperty(name)) {
                const misc = Assets.Misc[name];
                if (!this.game.cache.checkBinaryKey(misc.name)) {
                    this.game.load.binary(misc.name, misc.file);
                }
            }
        }
    }
}
